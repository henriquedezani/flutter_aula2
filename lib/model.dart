import 'dart:async';
import 'dart:io' as io;

import 'package:contatos_app/contato.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';


abstract class Model {

  static Database _db;

  Future<Database> get db async {
    if(_db != null) {
      return _db;
    }
    else {
      _db = await _initDb();
      return _db;
    }
  }

  _initDb() async { 
    io.Directory diretorio = await getApplicationDocumentsDirectory();
    String path = join(diretorio.path, 'contatos.db');
    return await openDatabase(path, version: 1, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  void _onCreate(Database db, int version) { 
    String sql = 'CREATE TABLE IF NOT EXISTS contato (id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, telefone TEXT, foto TEXT)';
    db.execute(sql);
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) {

  }
}

class ContatoModel extends Model { 

  Future<List<Contato>> read() async { 
    
    Database database = await db;

    List<Contato> contatos = new List<Contato>();

    List<Map> lista = await database.rawQuery("SELECT * FROM contato");

    for(int i = 0; i < lista.length; i++) { 
      Contato c = new Contato(lista[i]['nome'], lista[i]['telefone'], id: lista[i]['id'], foto: lista[i]['foto']);
      contatos.add(c);
    }

    return contatos;
  }

  void create(Contato c) async { 

    Database database = await db;

    String sql = "INSERT INTO contato (nome, telefone, foto) VALUES (?, ?, ?)";

    await database.rawInsert(sql, [c.nome, c.telefone, c.foto]);
  }

  void update(Contato c) async { 
    Database database = await db;

    String sql = "UPDATE contato SET nome = ?, telefone = ? WHERE id = ?";

    await database.rawUpdate(sql, [c.nome, c.telefone, c.id]);
  }

  void delete(int id) async {
    Database database = await db;

    String sql = "DELETE FROM contato WHERE id = ?";

    await database.rawDelete(sql, [id]);
  }

}
