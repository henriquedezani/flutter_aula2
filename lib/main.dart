import 'dart:io';

import 'package:contatos_app/cadastro.dart';
import 'package:contatos_app/contato.dart';
import 'package:contatos_app/model.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() { 
  runApp(new App());
}

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Meus Contatos',
      home: new ListaScreen()
    );
  } 
}

class ListaScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new ListaScreenState();  
}

class ListaScreenState extends State<ListaScreen> {

  Widget getItem(Contato contato) {
    return GestureDetector(
      onTap: () { 
        launch('tel:${contato.telefone}');
      },
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.only(bottom: 12.0),
        child: new Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 12.0),
              child: CircleAvatar(
                backgroundImage: contato.foto == null ? AssetImage('images/foto.png') : FileImage(File(contato.foto))
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(contato.nome, style: TextStyle(fontWeight: FontWeight.bold),),
                  Text(contato.telefone)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contatos"),
      ),
      body: Container(
        padding: EdgeInsets.all(12.0),
        child: FutureBuilder(
          future: new ContatoModel().read(),
          builder: (context, snapshot) { 
            if(snapshot.hasData) { 
              // crio a listview
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) { 
                  return getItem(snapshot.data[index]);
                },
              );
            }
            else {
              return new Text("Carregando...");
            }
          },
        )
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () { 
          Navigator.of(context).push(
            new MaterialPageRoute(
              builder: (_) => new CadastroScreen() 
              
            ));
        },
        child: Icon(Icons.add),
      ),
    );
  } 

}