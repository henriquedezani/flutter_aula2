import 'dart:async';
import 'dart:io';

import 'package:contatos_app/contato.dart';
import 'package:contatos_app/model.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CadastroScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new CadastroScreenState();
  } 
}

class CadastroScreenState extends State<CadastroScreen> {

  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  // Key _formKey = new Key<FormState>();

  String nome;
  String telefone;
  File foto;

  Future<void> tirarFoto() async { 
    File _foto = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      this.foto = _foto; 
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Novo Contato")
      ),
      body: Form(
        key: _formKey,
        autovalidate: false,
        child: ListView(
          padding: EdgeInsets.all(12.0),
          children: <Widget>[

            GestureDetector(
              onTap: tirarFoto,
              child: Container(
                color: Colors.white,
                child: foto == null ? Image.asset('images/foto.png') : Image.file(foto),
              ),
            ),

            TextFormField(
              decoration: InputDecoration(
                labelText: "Nome",
                hintText: "Nome completo"
              ),
              validator: (String value) {
                if(value.isEmpty) {
                  return 'Campo nome obrigatório';
                }
              },
              onSaved: (String value) { 
                this.nome = value;
              },
            ),
            TextFormField(
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                labelText: "Telefone",
                hintText: "(17) 3333-2211"
              ),
              validator: (String value) {
                if(value.isEmpty) {
                  return 'Campo telefone obrigatório';
                }
              },
              onSaved: (String value) { 
                this.telefone = value;
              },
            ),
            Container(
              child: RaisedButton(
                onPressed: () {
                  FormState form = _formKey.currentState;
                  if(form.validate()) {
                    form.save();
                    Contato c = new Contato(this.nome, this.telefone, foto: this.foto.path);
                    ContatoModel model = new ContatoModel();
                    model.create(c);
                    Navigator.of(context).pop();
                  }
                },
                child: Text("SALVAR"),
                color: Colors.blue,
                textColor: Colors.white,
              ),
            )
          ],
        ),
      )
    );
  } 
}